#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"

void MainWindow::FillMenuFileDialog()
{
    QMenu * fileDialog = new QMenu("Файл");
    fileDialog->addAction("Открыть файл",
                        this,
                        SLOT(OpenFile()),
                        Qt::CTRL + Qt::Key_O
                       );
    fileDialog->addAction("Сохранить файл",
                        this,
                        SLOT(SaveFile()),
                        Qt::CTRL + Qt::Key_S
                       );
    ui->menuBar->addMenu(fileDialog);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    stringFromFile="";
    connect(ui->inputTextEdit, SIGNAL(textChanged()), this, SLOT(Translate()));
    FillMenuFileDialog();
    FillMapLatinToMorse();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::FillMapLatinToMorse()
{
    mapLatinToMorse['a']=".-";
    mapLatinToMorse['b']="-...";
    mapLatinToMorse['c']="-.-.";
    mapLatinToMorse['d']="-..";
    mapLatinToMorse['e']=".";
    mapLatinToMorse['f']="..-.";
    mapLatinToMorse['g']="--.";
    mapLatinToMorse['h']="....";
    mapLatinToMorse['i']="..";
    mapLatinToMorse['j']=".---";
    mapLatinToMorse['k']="-.-";
    mapLatinToMorse['l']=".-..";
    mapLatinToMorse['m']="--";
    mapLatinToMorse['n']="-.";
    mapLatinToMorse['o']="---";
    mapLatinToMorse['p']=".--.";
    mapLatinToMorse['q']="--.-";
    mapLatinToMorse['r']=".-.";
    mapLatinToMorse['s']="...";
    mapLatinToMorse['t']="-";
    mapLatinToMorse['u']="..-";
    mapLatinToMorse['v']="...-";
    mapLatinToMorse['w']=".--";
    mapLatinToMorse['x']="-..-";
    mapLatinToMorse['y']="-.--";
    mapLatinToMorse['z']="--..";
}

bool MainWindow::IsLatin(QString checkString)
{
    /*
     * функция циклом идет по полученной строке и если находит в ней хотя бы
     * один символ, который является ключом mapLatinToMorse
     * возвращает true, иначе false
     */
    bool isLatin = false;
    int countChars = checkString.count();
    checkString = checkString.toLower();//переводим символы строки в строчные символы
    for(int i=0; i<=countChars;i++)
    {
        QChar currentChar = checkString[i];
        if(mapLatinToMorse.contains(currentChar))
        {
            isLatin=true;
            return isLatin;
        }
    }
    return isLatin;
}

QString MainWindow::LatinToMorse(QString string)
{
    //циклом идет по символам строки и переводит их в коды Морзе
    QString translatedString = "";//строка, которая будем переводом
    int countChars =string.count();//количество символов в строке
    for(int i=0;i<countChars;i++)
    {
        QChar currentChar = string[i];

        if(mapLatinToMorse.contains(currentChar))//если символ латиницы
        {
            translatedString.append(mapLatinToMorse[currentChar]);//получаем его код Морзе
            translatedString.append(" ");//и добавляем пробел после него
        }
        else if(currentChar=='\n')//если конец строки
            translatedString.append("\n");//в перевод добавляем символ конца строки
        else if (currentChar==' ')//если пробел
            translatedString.append(" ");//добавляем пробел
    }
    return translatedString;
}

QString MainWindow::MorseToLatin(QString string)
{
    QString translatedString = "";
    QStringList splitList = string.split(" ");//сплитим строку по пробелу
    int countChars = splitList.count();//получаем количество символов латиницы в кодах Морзе из переданной строки
    for(int i=0; i<countChars; i++)//идем по полученным строкам после сплита
    {
        QString currentChar = splitList[i];
        if(currentChar.isEmpty())//если пустая строка - значит это пробел
            translatedString.append(" ");
        else
        {
            if(mapLatinToMorse.key(currentChar,'0')!='0')//ищем ключ по значению, если такого значения нет, возвращаем символ '0'
            {
                translatedString.append(mapLatinToMorse.key(currentChar));//если символ есть, добавляем его в возвращаемую строку
            }
        }
    }
    return translatedString;
}

void MainWindow::Translate()
{
    QString translatedString = "";
    QString string="";

    if(stringFromFile.isEmpty())//если не был открыт файл
        string = ui->inputTextEdit->toPlainText();//берем строку из поля ввода
    else //если файл был открыт
    {
        /*
         * отсоединяем сигнал от этого слота,
         * так как при setPlainText происходит вызов сигнала textChanged()
         * и, соотвественно, вызывается этот слот
         */
        disconnect(ui->inputTextEdit, SIGNAL(textChanged()), this, SLOT(Translate()));


        string = stringFromFile;
        stringFromFile.clear();//очищаем строку,иначе будет мусор при следующем открытии файла
        ui->inputTextEdit->setPlainText(string);
       //снова соединяем этот слот с сигналом textChanged()
        connect(ui->inputTextEdit, SIGNAL(textChanged()), this, SLOT(Translate()));

    }

    string = string.toLower();//переводим в строчные символы
    if(IsLatin(string))
    {
        translatedString=LatinToMorse(string);
    }
    else
    {
        translatedString=MorseToLatin(string);
    }
    ui->outputTextEdit->clear();
    ui->outputTextEdit->insertPlainText(translatedString);//отображаем результат перевоада
}

void MainWindow::OpenFile()
{
    QString fromFile = "";

    QString fileName = QFileDialog::getOpenFileName(this, tr("Открыть файл"),QDir::homePath());
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly))
    {
        QByteArray data;
        data = file.readAll();
        fromFile.append(data);
        if(!fromFile.isEmpty())
        {
            stringFromFile = fromFile;
            ui->inputTextEdit->clear();
            ui->outputTextEdit->clear();

            Translate();
        }
        else
            QMessageBox::warning(this,"Внимание!","Файл пустой!");
        file.close();
    }



}

void MainWindow::SaveFile()
{
    QString stringToFile = ui->outputTextEdit->toPlainText();
    if(!stringToFile.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл"),QDir::homePath());
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly))
        {
            QByteArray data;
            data = stringToFile.toUtf8();
            file.write(data,data.count());
            QMessageBox::warning(this,"Внимание!","Файл успешно записан!");
            file.close();
        }
    }

}


void MainWindow::on_clearButton_clicked()
{
    ui->inputTextEdit->clear();
    ui->outputTextEdit->clear();

}

void MainWindow::on_copyInputButton_clicked()
{
    QString clipString = ui->inputTextEdit->toPlainText();
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(clipString);
}

void MainWindow::on_copyOutputButton_clicked()
{
    QString clipString = ui->outputTextEdit->toPlainText();
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(clipString);
}
