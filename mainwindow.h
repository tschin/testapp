#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QClipboard>




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    //словарь для ассоциаций кодов Морзе и латинских символов
    QMap<QChar, QString> mapLatinToMorse;
    //Заполняет словарь значениями
    void FillMapLatinToMorse();
    //проверяет какие язык ввода: латинские символы или коды Морзе
    bool IsLatin(QString checkString);
    //перевод из латинских символов в коды Морзе
    QString LatinToMorse(QString string);
    //перевод кодов Морзе в латинские символы
    QString MorseToLatin(QString string);
    //создание и заполнение меню Файл
    void FillMenuFileDialog();
    //Эта строка заполняется из открытого файла и потом используется в слоте Translate()
    QString stringFromFile;

private slots:
    //функция перевода
    void Translate();

    void OpenFile();
    void SaveFile();

    void on_clearButton_clicked();
    void on_copyInputButton_clicked();
    void on_copyOutputButton_clicked();
};

#endif // MAINWINDOW_H
